dotvim
======
Pull the repo:
```
git clone https://github.com/Garbee/dotvim.git ~/.vim
```

Install vundle:
```
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
```

Link the vimrc file to your .vimrc:
```
ln -s ~/.vim/vimrc ~/.vimrc
```

Install plugins:
```
vim -c ':BundleInstall' -c 'qa!'
```

Setup phpactor:
```
vim -c ':call phpactor#Update()' -c 'qa!'
```

