set shell=bash
set nocompatible
set encoding=utf-8
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'xolox/vim-misc'
Plugin 'tpope/vim-fugitive'
Plugin 'scrooloose/nerdtree'
Plugin 'scrooloose/syntastic'
Plugin 'tpope/vim-surround'
Plugin 'kien/ctrlp.vim'
Plugin 'bling/vim-airline'
Plugin 'majutsushi/tagbar'
Plugin 'scrooloose/nerdcommenter'
Plugin 'airblade/vim-gitgutter'
Plugin 'pangloss/vim-javascript'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'easymotion/vim-easymotion'
Plugin 'godlygeek/tabular'
Plugin 'plasticboy/vim-markdown'
Plugin 'othree/html5.vim'
Plugin 'mxw/vim-jsx'
Plugin 'elzr/vim-json'
Plugin 'editorconfig/editorconfig-vim'
Plugin 'junegunn/gv.vim'
Plugin 'yuttie/comfortable-motion.vim'
Plugin 'jistr/vim-nerdtree-tabs'
Plugin 'xuyuanp/nerdtree-git-plugin'
Plugin 'ntpeters/vim-better-whitespace'
Plugin 'moll/vim-node'
Plugin 'ekalinin/dockerfile.vim'
Plugin 'dag/vim-fish'
Plugin 'evidens/vim-twig'
Plugin 'nginx.vim'
Plugin 'phpactor/phpactor'

call vundle#end()
filetype plugin indent on
let g:easytags_events = ['BufReadPost', 'BufWritePost']
let g:easytags_async = 1
let g:easytags_dynamic_files =2
let g:easytags_resolve_links = 1
let g:easytags_supress_ctags_warning = 1
set showmode
set nowrap
set tabstop=4
set ruler
set hlsearch
set incsearch
set lazyredraw
set showmatch
set ffs=unix,dos,mac
set nobackup
set nowb
set noswapfile
set smarttab
set tags=tags
set softtabstop=4
set expandtab
set shiftwidth=4
set shiftround
set backspace=indent,eol,start
set autoindent
set copyindent
set number
set ignorecase
set smartcase
set visualbell
set noerrorbells
set autowrite
set smarttab
set ai
set si
set guioptions-=T
set guioptions-=r
set background=dark
set linespace=25

"" Keymap
" set leader to comma
let mapleader = ","
let g:mapleader = ","

" Tab shortcuts
map <leader>tn :tabnew<cr>
map <leader>to :tabonly<cr>
map <leader>tc :tabclose<cr>
map <leader>tm :tabmove<cr>
nmap <C-j> :tabprev<CR>
nmap <C-h> :tabnext<CR>
map <C-n> <Esc>:tabnew

map <C-n> :NERDTreeToggle<CR>
nmap <C-b> :NERDTreeTabsToggle<CR>
nmap <leader>t :TagbarToggle<CR>
imap jj <esc>
" Reindent file
map <F7> mzgg=G`z<CR>

"" PHPactor mappings
""" Include use statement
map <Leader>u :call phpactor#UseAdd()<CR>
map <Leader>o :call phpactor#GotoType()<CR>
map <Leader>pd :call phpactor#OffsetTypeInfo()<CR>
map <Leader>i :call phpactor#ReflectAtOffset()<CR>
map <Leader>pfm :call phpactor#MoveFile()<CR>
map <Leader>pfc :call phpactor#CopyFile()<CR>
map <Leader>tt :call phpactor#Transform()<CR>
map <Leader>cc :call phpactor#ClassNew()<CR>
""" Show information about "type" under cursor including current frame
nnoremap <silent><Leader>d :call phpactor#OffsetTypeInfo()<CR>

" Don't require JSX file extension for highlighting
let g:jsx_ext_required = 0
" Ignore editorconfig for fugitive
let g:EditorConfig_exclude_patterns = ['fugitive://.*']
" Don't fold markdown files
let g:vim_markdown_folding_disabled = 1


" Detect file types
au! BufRead,BufNewFile *.json set filetype=json

function! <SID>StripTrailingWhitespaces()
    " Preparation: save last search, and curor position.
    let _s=@/
    let l = line(".")
    let c = col(".")
    " Do the business:
    %s/\s\+$//e
    "Clean up: Restory prev search history and cursor position.
    let @/=_s
    call cursor(l,c)
endfunction

set listchars=tab:▸\ ,eol:¬

" Syntastic config
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_aggregate_errors = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_javascript_checkers = ['eslint']
let g:syntastic_php_checkers = ['php', 'phpcs', 'phpmd']
let g:syntastic_php_phpcs_args='--standard=PSR2'

if has('autocmd')
    autocmd bufwritepost .vimrc source $MYVIMRC
    autocmd BufWritePre *.php,*.js,*.css,*.html :call <SID>StripTrailingWhitespaces()
    autocmd Filetype html setlocal ts=2 sts=2 sw=2
    autocmd BufRead,BufNewFile /etc/nginx/sites-*/* setfiletype nginx
    autocmd FileType fish compiler fish
    autocmd FileType fish setlocal textwidth=79
    autocmd FileType php setlocal omnifunc=phpactor#Complete
endif

